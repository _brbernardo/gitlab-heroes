# How the steering committee can help to achieve/enhance this

This page is currently a work in progress. Reference: https://gitlab.com/gitlab-com/marketing/community-relations/gitlab-heroes/issues/35

This section is inspired by @nico-meisenzahl comment here https://gitlab.com/gitlab-com/marketing/community-relations/gitlab-heroes/issues/35#note_280537700 and contains tasks and duties of the Steering committee to help the Heroes program achieve its objectives.
* Sending out feedback or thank you notes to Heroes for their contributions (especially new contributors) and when they graduate to another level in the program such as from Hero to Superheroes.)
* Documenting what gets discussed in the steering committee meetings and soliciting ideas from other Heroes on what should be prioritized and what challenges they face as contributors 
