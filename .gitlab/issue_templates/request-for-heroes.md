# Request for Heroes 

Please use this template when sharing contribution opportunities with our GitLab Heroes. Opportunities may include asking GitLab Heroes to attend events, submit CFPs, deliver tech talks, write blog posts, record videos, etc. 

GitLab Heroes: please indicate your interest in these opportunities in the comments. Once a GitLab Hero is selected, please open an issue using the [heroes-content template](https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/issues/new?issuable_template=heroes-content) for planning and tracking purposes.

## Request Details

- Name of requestor: 
- Request Type: 
  - [ ] CFP Submission 
  - [ ] Conference / Meetup Talk
  - [ ] Blog post
  - [ ] Video 
  - [ ] Other  
- Description: 

## Event Details (for event-related requests)
- Name of event: 
- Event website: 
- Location: 
- Event Date: 
- CFP Deadline: 
- CFP website: 

## Related issues
<!-- Please link to relevant issues in this section -->

/label ~Heroes ~"request-for-heroes" 
