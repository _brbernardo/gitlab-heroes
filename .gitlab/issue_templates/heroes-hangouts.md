<!-- Title of issue should be: Heroes Virtual Hangout: YYYY/MM/DD TIMEUTC Ex: "Heroes Virtual Hangout: 2021/03/17 2000UTC" -->

Please use this template when proposing or scheduling to host one of the monthly GitLab Heroes virtual hangouts. 

The purpose of monthly hangouts is to bring together the Heroes to network, share hacks and build comradery amongst the GitLab hereos.  As the evangelism program manager, one of my goals is to get to know more of the GitLab Heroes and engage with you all on a regular basis.

## Event  Details

* **Names of hero host(s) | include GitLab handle**: 
* **Date**: (_Perferably the 3rd Thursday of the month_)
* **Time**:
* **Region**:
* **Link to join**: 
* **Notes**:

## Hot discussion topics or questions: (these will be added to agenda)
1. ex. Name: Topic
1. 
1.
1.

## Tasks

* [ ] Update and share agenda [Google Doc](https://docs.google.com/document/d/1uuwzcbg3IAXVm2Z8Yzcuh8d8GCrh5_20g2NtS4KdyE8/edit#heading=h.za3ga0ol0xwe)
* [ ] Assign this issue to the host 
* [ ] Create meeting link or request a Zoom link from @jrachel1 in a comment on this issue 
* [ ] Please share in the GitLab Heroes channel on Gitter: [Heroes channel](https://gitter.im/gitlab/heroes )

/assign @jrachel1
/label ~Heroes 
/confidential  <!-- These issues are confidential to limit risk of Zoom bombing -->



