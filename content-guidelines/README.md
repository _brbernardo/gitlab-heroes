# Guidelines and course topics to be discussed in meetups, videos, blogs and within the Hero platform

This page is currently a work in progress. Reference: https://gitlab.com/gitlab-com/marketing/community-relations/gitlab-heroes/issues/35

This section is inspired by @VladimirAus comment https://gitlab.com/gitlab-com/marketing/community-relations/gitlab-heroes/issues/35#note_281126226 and contains course topics that can be used to educate a Hero on what to talk about in a meetup and also help to spread ideas for novice organizers to borrow from.
* Introduction to Gitlab CI. 
* How to contribute to GitLab. 
* How to organize a meetup.
* Gitlab for non-developer series
